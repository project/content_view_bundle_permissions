<?php

/**
 * @file
 * This file implements hooks for the content_view_bundle_permissions bundle.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Alter the query of views.
 *
 * @see \hook_views_query_alter()
 */
function content_view_bundle_permissions_views_query_alter(
  ViewExecutable $view,
  QueryPluginBase $query,
): void {
  /** @var \Drupal\content_view_bundle_permissions\Service\Hook\ViewsQueryAlter $views_query_alter */
  $views_query_alter = Drupal::service(
    'content_view_bundle_permissions.views_query_alter'
  );
  $views_query_alter->alter($view, $query);
}

/**
 * Alter the exposed form of views.
 *
 * @see \hook_form_FORM_ID_alter()
 */
function content_view_bundle_permissions_form_views_exposed_form_alter(
  &$form,
  FormStateInterface $form_state,
  $form_id,
): void {
  /** @var \Drupal\content_view_bundle_permissions\Service\Hook\FormViewsExposedFormAlter $form_views_exposed_form_alter */
  $form_views_exposed_form_alter = Drupal::service(
    'content_view_bundle_permissions.form_views_exposed_form_alter'
  );
  $form_views_exposed_form_alter->alter($form, $form_state);
}
