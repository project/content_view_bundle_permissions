<?php

namespace Drupal\content_view_bundle_permissions\Service\Hook;

use Drupal\content_view_bundle_permissions\ContentViewBundlePermissions;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\ViewExecutable;

/**
 * Hook implementation for altering the content view exposed form.
 */
class FormViewsExposedFormAlter {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Hide bundle options if the user does not have permission.
   */
  public function alter(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $view = $form_state->get('view');
    if (!$view instanceof ViewExecutable || $view->id() !== 'content') {
      return;
    }
    $node_bundle_infos = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($node_bundle_infos as $bundle => $info) {
      $has_any_permission = $this->currentUser->hasPermission(
        ContentViewBundlePermissions::getAnyPermission($bundle),
      );
      $has_own_permission = $this->currentUser->hasPermission(
        ContentViewBundlePermissions::getOwnPermission($bundle),
      );
      if ($has_any_permission || $has_own_permission) {
        continue;
      }
      unset($form['type']['#options'][$bundle]);
    }
  }

}
