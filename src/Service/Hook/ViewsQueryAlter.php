<?php

namespace Drupal\content_view_bundle_permissions\Service\Hook;

use Drupal\content_view_bundle_permissions\ContentViewBundlePermissions;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Hook implementation for altering the content view query.
 */
class ViewsQueryAlter {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly AccountProxyInterface $currentUser,
  ) {}

  /**
   * Exclude all or only non-owned nodes if the user does not have permission.
   */
  public function alter(ViewExecutable $view, QueryPluginBase $query): void {
    if ($view->id() !== 'content') {
      return;
    }
    $node_bundle_infos = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($node_bundle_infos as $bundle => $info) {
      $has_any_permission = $this->currentUser->hasPermission(
        ContentViewBundlePermissions::getAnyPermission($bundle),
      );
      if ($has_any_permission) {
        continue;
      }
      $has_own_permission = $this->currentUser->hasPermission(
        ContentViewBundlePermissions::getOwnPermission($bundle),
      );
      $group_id = $query->setWhereGroup('OR');
      $query->where[$group_id]['conditions'][] = [
        'field' => 'node_field_data.type',
        'value' => $bundle,
        'operator' => '!=',
      ];
      if ($has_own_permission) {
        $query->where[$group_id]['conditions'][] = [
          'field' => 'node_field_data.uid',
          'value' => $this->currentUser->id(),
          'operator' => '=',
        ];
      }
    }
  }

}
