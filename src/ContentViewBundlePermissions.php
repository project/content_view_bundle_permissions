<?php

namespace Drupal\content_view_bundle_permissions;

use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;

/**
 * This class implements the permissions callback of this module.
 */
class ContentViewBundlePermissions {

  use BundlePermissionHandlerTrait;

  use StringTranslationTrait;

  /**
   * Get the permission string to view any content of a specific bundle.
   */
  public static function getAnyPermission(string $type_id): string {
    return "view any $type_id in content view";
  }

  /**
   * Get the permission string to view own content of a specific bundle.
   */
  public static function getOwnPermission(string $type_id): string {
    return "view own $type_id in content view";
  }

  /**
   * This is the permission callback.
   *
   * @see BundlePermissionHandlerTrait::generatePermissions()
   */
  public function permissions(): array {
    return $this->generatePermissions(
      NodeType::loadMultiple(),
      [$this, 'buildPermissions'],
    );
  }

  /**
   * Build and return the permissions for a specific bundle.
   */
  protected function buildPermissions(NodeType $node_type): array {
    $type_id = $node_type->id();
    $t_args = [
      '%node_type_label' => $node_type->label(),
    ];
    return [
      self::getAnyPermission($type_id) => [
        'title' => $this->t(
          '%node_type_label: View any in content view',
          $t_args,
        ),
        'description' => $this->t(
          'Allows the user to view any content of this type in the content view.',
        ),
      ],
      self::getOwnPermission($type_id) => [
        'title' => $this->t(
          '%node_type_label: View own in content view',
          $t_args,
        ),
        'description' => $this->t(
          'Allows the user to view own content of this type in the content view.',
        ),
      ],
    ];
  }

}
